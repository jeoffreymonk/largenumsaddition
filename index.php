<?php

require_once 'NumbersGenerator\LargeNumbersGenerator.php';
require_once 'LargeNumbersSumTool\LargeNumsSum.php';
require_once 'LargeNumbersSumTool\BCMathLargeNumsSum.php';
require_once 'LargeNumbersSumTool\GMPLargeNumsSum.php';
require_once 'LargeNumbersSumTool\ShellLargeNumsSum.php';

const NUMBER_LENGTH = 50;
const NUMBERS_ARRAY_COUNT = 50;

// 1-й способ: сложить числа вручную
function sumNumbersManually(): string
{
    $largeNumbersGenerator = new LargeNumbersGenerator();
    $numbersArray = $largeNumbersGenerator->generateNumbersArray(NUMBER_LENGTH, NUMBERS_ARRAY_COUNT);

    $largeNumsSum = new LargeNumsSum();

    $largeNumsSum->setNumbers($numbersArray);

    return $largeNumsSum->sumNumbers();
}

// 2-й способ: сложить числа, используя модуль BCMath
function bcMathNumbersSum(): string
{
    $largeNumbersGenerator = new LargeNumbersGenerator();
    $numbersArray = $largeNumbersGenerator->generateNumbersArray(NUMBER_LENGTH, NUMBERS_ARRAY_COUNT);

    $bcMathLargeNumsSum = new BCMathLargeNumsSum();

    $bcMathLargeNumsSum->setNumbers($numbersArray);

    return $bcMathLargeNumsSum->sumNumbers();
}

// 3-й способ: сложить числа, используя модуль GMP
function gmpNumbersSum(): string
{
    $largeNumbersGenerator = new LargeNumbersGenerator();
    $numbersArray = $largeNumbersGenerator->generateNumbersArray(NUMBER_LENGTH, NUMBERS_ARRAY_COUNT, true);

    $gmpLargeNumsSum = new GMPLargeNumsSum();

    $gmpLargeNumsSum->setNumbers($numbersArray);

    return $gmpLargeNumsSum->sumNumbers();
}

// 4-й способ: сложить числа, используя утилиту bc командной оболочки Shell
function shellNumbersSum(): string
{
    $largeNumbersGenerator = new LargeNumbersGenerator();
    $numbersArray = $largeNumbersGenerator->generateNumbersArray(NUMBER_LENGTH, NUMBERS_ARRAY_COUNT);

    $shellLargeNumsSum = new ShellLargeNumsSum();

    $shellLargeNumsSum->setNumbers($numbersArray);

    return $shellLargeNumsSum->sumNumbers();
}

//print sumNumbersManually() . '<br>';
//print bcMathNumbersSum() . '<br>';
//print gmpNumbersSum() . '<br>';
//print shellNumbersSum() . '<br>';