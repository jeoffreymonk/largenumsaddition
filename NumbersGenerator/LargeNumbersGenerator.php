<?php

class LargeNumbersGenerator
{
    private const MIN_RANGE     = 0;
    private const MAX_RANGE     = 9;
    private const GMP_MIN_RANGE = "10000000000000000000000000000000000000000000000000";
    private const GMP_MAX_RANGE = "99999999999999999999999999999999999999999999999999";

    public function generateLargeNumber(int $numberLength): string
    {
        $number = '';

        for ($i = 1; $i <= $numberLength; $i++) {
            if (!$number) {
                $generateNumber = $this->generateDigit();

                $generateNumber > 0 ? $number = $generateNumber : $i = -1;
            } else {
                $number .= $this->generateDigit();
            }
        }

        return $number;
    }

    public function generateGMPNumber(): GMP
    {
        return gmp_random_range($this::GMP_MIN_RANGE, $this::GMP_MAX_RANGE);
    }

    public function generateNumbersArray(int $numberLength, int $arrayCount, $gmpNumbers = false): array
    {
        $numbers = [];

        for ($i = 1; $i <= $arrayCount; $i++) {
            $numbers[] = $gmpNumbers ? $this->generateGMPNumber() : $this->generateLargeNumber($numberLength);
        }

        return $numbers;
    }

    private function generateDigit(): int
    {
        return mt_rand($this::MIN_RANGE, $this::MAX_RANGE);
    }
}
