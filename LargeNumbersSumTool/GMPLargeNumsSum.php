<?php

require_once 'LargeNumsSum.php';

class GMPLargeNumsSum extends LargeNumbersSumHelper
{
    public function sumNumbers(): string
    {
        foreach ($this->numbers as $number) {
            $this->sum = gmp_strval(gmp_add($this->sum, $number));
        }

        return $this->sum;
    }
}
