<?php

require_once 'LargeNumsSum.php';

class ShellLargeNumsSum extends LargeNumbersSumHelper
{

    public function sumNumbers(): string
    {
        foreach ($this->numbers as $number) {
            $this->sum = shell_exec("echo $this->sum + $number |bc");
        }

        return $this->sum;
    }
}
