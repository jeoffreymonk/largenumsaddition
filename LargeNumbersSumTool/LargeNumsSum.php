<?php

require_once 'LargeNumbersSumHelper.php';

class LargeNumsSum extends LargeNumbersSumHelper
{
    public function sumNumbers(): string
    {
        foreach ($this->numbers as $number) {
            if ($this->sum === "0") {
                $this->sum = $number;
            } else {
                $this->sum = $this->sumTwoNumbers($this->sum, $number);
            }
        }

        return $this->sum;
    }

    private function sumTwoNumbers(string $firstNum, string $secondNum): string
    {
        $firstNumArray = array_reverse(str_split($firstNum));
        $secondNumArray = array_reverse(str_split($secondNum));
        $minNumbersArrayCount = min(count($firstNumArray), count($secondNumArray));
        $result = [];
        $additionalDigit = 0;

        for ($i = 0; $i <= $minNumbersArrayCount - 1; $i++) {
            if (!isset($firstNumArray[$i])) {
                $result[] = $secondNumArray[$i] + $additionalDigit;

                break;
            }

            if (!isset($secondNumArray[$i])) {
                $result[] = $firstNumArray[$i] + $additionalDigit;

                break;
            }

            list($sum, $addNum) = $this->sumTwoDigits($firstNumArray[$i], $secondNumArray[$i], $additionalDigit);

            $result[] = $sum;
            $additionalDigit = $addNum;

            if ($i === $minNumbersArrayCount - 1 && $additionalDigit > 0) {
                $result[] = $additionalDigit;
            }
        }

        return implode('', array_reverse($result));
    }

    private function sumTwoDigits(int $firstDigit, int $secondDigit, int $additionalDigit = 0): array
    {
        $sum = strval($firstDigit + $secondDigit + $additionalDigit);

        if ($sum >= 10) {
            $additionalDigit = substr($sum, 0, 1);
            $result = substr($sum, 1, 1);
        } else {
            $additionalDigit = 0;
            $result = $sum;
        }

        return [$result, $additionalDigit];
    }
}
