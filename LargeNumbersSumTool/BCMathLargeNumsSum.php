<?php

require_once 'LargeNumsSum.php';

class BCMathLargeNumsSum extends LargeNumbersSumHelper
{
    public function sumNumbers(): string
    {
        foreach ($this->numbers as $number) {
            $this->sum = bcadd($number, $this->sum);
        }

        return $this->sum;
    }
}
