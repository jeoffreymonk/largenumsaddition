<?php

abstract class LargeNumbersSumHelper
{
    /**
     * @var array
     */
    protected $numbers;
    /**
     * @var string
     */
    protected $sum = '0';

    public function getNumbers(): array
    {
        return $this->numbers;
    }

    public function setNumbers(array $numbers): void
    {
        $this->numbers = $numbers;
    }

    public function getSum(): string
    {
        return $this->sum;
    }

    public function setSum(string $sum): void
    {
        $this->sum = $sum;
    }

    public abstract function sumNumbers(): string;
}
